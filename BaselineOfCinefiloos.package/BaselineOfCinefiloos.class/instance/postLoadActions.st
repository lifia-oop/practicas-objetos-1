baseline
postLoadActions
	"Start a Server on 8080 and Setup Repository"

	GTPlayground
		edit:
			'"If MongoDB is available in your local machine, evaluate the following
expression."
(VOMongoRepository
   host: ''localhost''
   database: ''cinefiloos'') enableSingleton .

"If you prefer to simulate the dababase in memory, run the following
expression instead."
VOMemoryRepository new  enableSingleton.
			
"To install the Cinefiloos application, run the following expresion"
(WAAdmin register: CinefilosHomeComponent asApplicationAt: ''cinefiloos'')
      addLibrary: JQDeploymentLibrary;
      addLibrary:  TBSDeploymentLibrary.
WAAdmin register: CinefiloosApi at: ''cinefiloos-api''.
ZnZincServerAdaptor startOn: 8080	

"You will fine cinefiloos at: http://localhost:8080/cinefiloos"'
		label: 'Cinefiloos'