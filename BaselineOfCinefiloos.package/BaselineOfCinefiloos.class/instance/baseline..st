baseline
baseline: spec
	<baseline>
	spec
		for: #common
		do: [ self
				seaside: spec;
				bootstrap: spec;
				voyage: spec.
			spec
				package: 'Objetos1-Cinefiloos'
				with: [ spec requires: #('Seaside3' 'Bootstrap-Core' 'Voyage') ].
			spec postLoadDoIt: #postLoadActions ]