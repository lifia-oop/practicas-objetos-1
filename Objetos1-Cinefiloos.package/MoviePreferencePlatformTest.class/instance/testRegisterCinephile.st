testing
testRegisterCinephile
	| cinefiloos cinephile |
	cinefiloos := CollectionsBasedMoviePreferencePlatform new.
	cinephile := cinefiloos
		registerCinephileNamed: 'Juan de los Palotes'
		withEmail: 'juan@mail.com'.
	self assert: cinefiloos cinephiles size equals: 1.
	self assert: cinefiloos cinephiles first fullname equals: 'Juan de los Palotes'.
	self assert: cinefiloos cinephiles first email equals: 'juan@mail.com'