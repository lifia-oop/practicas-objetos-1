testing
testUnseenMoviesInterestingTo
	| cinefiloos juan unaParaJuan unaDeMiedo unaQueJuanVio |
	cinefiloos := CollectionsBasedMoviePreferencePlatform new.
	juan := cinefiloos registerCinephileNamed: 'Juan de los Palotes' withEmail: 'juan@mail.com'.
	unaDeMiedo := cinefiloos addMovieTitled: 'Mucho miedo, solo miedo' withImdbUrl: '' andImageUrl: ''.
	unaDeMiedo genreProfile: GenreProfile justHorror.
	unaParaJuan := cinefiloos addMovieTitled: 'Una promedio... ' withImdbUrl: '' andImageUrl: ''.
	unaParaJuan genreProfile: juan genreProfile.
	unaQueJuanVio := cinefiloos addMovieTitled: 'Otra pelicula promedio' withImdbUrl: '' andImageUrl: ''.
	unaQueJuanVio genreProfile: juan genreProfile.
	juan watched: unaQueJuanVio.
	self assert: cinefiloos movies size equals: 3.
	self assertCollection: (cinefiloos unseenMoviesInterestingTo: juan) asSet equals: {unaParaJuan} asSet