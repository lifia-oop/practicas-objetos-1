testing
testCinephilesSimilarTo
	| cinefiloos juan parecidaAJuan distintaAJuan unaDeMiedo |
	cinefiloos := CollectionsBasedMoviePreferencePlatform new.
	juan := cinefiloos registerCinephileNamed: 'Juan de los Palotes' withEmail: 'juan@mail.com'.
	parecidaAJuan := cinefiloos registerCinephileNamed: 'Juana de los Palitos' withEmail: 'juana@mail.com'.
	distintaAJuan := cinefiloos registerCinephileNamed: 'Maria de los Dolores' withEmail: 'maria@mail.com'.
	unaDeMiedo := cinefiloos addMovieTitled: 'Mucho miedo, solo miedo' withImdbUrl: '' andImageUrl: ''.
	unaDeMiedo genreProfile: GenreProfile justHorror.
	distintaAJuan watched: unaDeMiedo.
	self
		assertCollection: (cinefiloos cinephilesSimilarTo: juan) asSet
		equals:
			{juan.
			parecidaAJuan} asSet