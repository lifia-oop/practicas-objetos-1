testing
testAddMovie
	| cinefiloos theTitle theImdbUrl theImageUrl movie |
	theTitle := 'TRON: Legacy'.
	theImdbUrl := 'https://www.imdb.com/title/tt1104001/?ref_=nv_sr_1'.
	theImageUrl := 'https://m.media-amazon.com/images/M/MV5BMTk4NTk4MTk1OF5BMl5BanBnXkFtZTcwNTE2MDIwNA@@._V1_SY1000_CR0,0,674,1000_AL_.jpg'.
	cinefiloos := CollectionsBasedMoviePreferencePlatform new.
	movie := cinefiloos
		addMovieTitled: theTitle
		withImdbUrl: theImdbUrl
		andImageUrl: theImageUrl.
	self assert: cinefiloos movies size equals: 1.
	self assert: cinefiloos movies first title equals: theTitle.
	self assert: cinefiloos movies first imdbUrl equals: theImdbUrl.
	self assert: cinefiloos movies first imageUrl equals: theImageUrl