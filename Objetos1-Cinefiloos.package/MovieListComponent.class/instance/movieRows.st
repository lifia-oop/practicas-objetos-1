private
movieRows
	movies ifEmpty: [ ^ Set new ].
	movies size < 3
		ifTrue: [ ^ Set with: movies ].
	^ (1 to: movies size by: 3) collect: [ :i | movies copyFrom: i to: (i + 2 min: movies size) ]