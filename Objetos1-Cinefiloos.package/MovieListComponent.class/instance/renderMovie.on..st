rendering
renderMovie: aMovie on: html
	html image
		resourceUrl: aMovie imageUrl;
		width: 200.
	html break.
	html paragraph: aMovie title.
	html
		paragraph: [ html
				strong: 'Horror: ';
				text: aMovie horrorProfile printString;
				break;
				strong: 'Comedy: ';
				text: aMovie comedyProfile printString;
				break;
				strong: 'Action: ';
				text: aMovie actionProfile printString;
				break;
				strong: 'Romance: ';
				text: aMovie romanceProfile printString;
				break;
				strong: 'Suspense: ';
				text: aMovie suspenseProfile printString;
				break;
				strong: 'Sci-Fi ';
				text: aMovie sciFiProfile printString;
				break ].
	(parent user hasWatched: aMovie)
		ifFalse: [ html
				paragraph: [ html text: 'Watched: '.
					html anchor
						callback: [ self theUserWatched: aMovie ];
						with: [ html tbsGlyphIcon iconUnchecked ] ] ]
		ifTrue: [ html
				paragraph: [ html text: 'Watched: '.
					html anchor
						callback: [ parent theUserForgot: aMovie ];
						with: [ html tbsGlyphIcon iconCheck ] ] ].
	html anchor
		callback: [ self moreLike: aMovie ];
		with: [ html paragraph: 'Find more like this' ]