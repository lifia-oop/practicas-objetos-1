rendering
renderContentOn: html
	self movieRows
		do: [ :someMovies | 
			html
				tbsRow: [ someMovies
						do: [ :aMovie | 
							html
								tbsColumn: [ html tbsThumbnail:
										[ self renderMovie: aMovie on: html ] ]
								mediumSize: 3 ] ] ]