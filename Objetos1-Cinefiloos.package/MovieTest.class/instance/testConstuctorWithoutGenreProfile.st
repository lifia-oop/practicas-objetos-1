testing
testConstuctorWithoutGenreProfile
	movie := Movie title: aTitle imdbUrl: anImdbUrl imageUrl: anImageUrl.
	self assert: movie title equals: aTitle.
	self assert: movie imdbUrl equals: anImdbUrl.
	self assert: movie imageUrl equals: anImageUrl.
	self assert: movie genreProfile equals: GenreProfile new