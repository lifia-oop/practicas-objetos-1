running
setUp
	aTitle := 'TRON: Legacy'.
	anImdbUrl := 'https://www.imdb.com/title/tt1104001/?ref_=nv_sr_1'.
	anImageUrl := 'https://m.media-amazon.com/images/M/MV5BMTk4NTk4MTk1OF5BMl5BanBnXkFtZTcwNTE2MDIwNA@@._V1_SY1000_CR0,0,674,1000_AL_.jpg'.
	movie := Movie
		title: aTitle
		imdbUrl: anImdbUrl
		imageUrl: anImageUrl
		genreProfile: GenreProfile new