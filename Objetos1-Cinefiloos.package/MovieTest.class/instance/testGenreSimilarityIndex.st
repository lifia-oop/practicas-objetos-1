testing
testGenreSimilarityIndex
	| aHorrorMovie anotheHorrorMovie aRomanceMovie |
	aHorrorMovie := Movie
		title: 'Hunted house'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justHorror.
	anotheHorrorMovie := Movie
		title: 'Hunted house 2'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justHorror.
	self assert: (aHorrorMovie genreSimilarityIndexTo: aHorrorMovie) equals: 0.
	self assert: (aHorrorMovie genreSimilarityIndexTo: anotheHorrorMovie) equals: 0.
	aRomanceMovie := Movie
		title: 'The funny movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justComedy.
	self assert: (aHorrorMovie genreSimilarityIndexTo: aRomanceMovie) equals: 18