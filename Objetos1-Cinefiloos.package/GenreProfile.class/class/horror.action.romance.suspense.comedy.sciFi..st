instance creation
horror: h action: a romance: r suspense: s comedy: c sciFi: sf
	^ self new
		horror: h
			action: a
			romance: r
			suspense: s
			comedy: c
			sciFi: sf;
		yourself