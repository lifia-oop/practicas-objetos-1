instance creation
asAnAverageOfAll: aCollectionOfProfiles
	| count |
	count := aCollectionOfProfiles size.
	^ self new
		horror: (aCollectionOfProfiles sum: [ :each | each horror ]) / count;
		action: (aCollectionOfProfiles sum: [ :each | each action ]) / count;
		romance: (aCollectionOfProfiles sum: [ :each | each romance ]) / count;
		suspense: (aCollectionOfProfiles sum: [ :each | each suspense ]) / count;
		comedy: (aCollectionOfProfiles sum: [ :each | each comedy ]) / count;
		sciFi: (aCollectionOfProfiles sum: [ :each | each sciFi ]) / count;
		yourself