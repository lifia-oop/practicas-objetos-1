comparing
= anotherGenreProfile
	^ horror = anotherGenreProfile horror & (action = anotherGenreProfile action)
		& (romance = anotherGenreProfile romance) & (suspense = anotherGenreProfile suspense)
		& (comedy = anotherGenreProfile comedy) & (sciFi = anotherGenreProfile sciFi)