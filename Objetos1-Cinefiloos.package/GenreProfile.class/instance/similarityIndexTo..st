comparing
similarityIndexTo: aGenreProfile
	^ (action - aGenreProfile action) abs + (comedy - aGenreProfile comedy) abs
		+ (horror - aGenreProfile horror) abs + (romance - aGenreProfile romance) abs
		+ (sciFi - aGenreProfile sciFi) abs + (suspense - aGenreProfile suspense) abs
		
	