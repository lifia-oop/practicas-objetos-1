utility
createExamples
	(self cinephileWithEmail: 'juan@palotes.com') ifNotNil: [ ^ self ].
	self registerCinephileNamed: 'Juan de los Palotes' withEmail: 'juan@palotes.com'.
	(self
		addMovieTitled: 'TRON: Legacy (2010)'
		withImdbUrl: 'https://www.imdb.com/title/tt1104001/?ref_=fn_al_tt_1'
		andImageUrl:
			'https://m.media-amazon.com/images/M/MV5BMTk4NTk4MTk1OF5BMl5BanBnXkFtZTcwNTE2MDIwNA@@._V1_SY1000_CR0,0,674,1000_AL_.jpg') genreProfile: GenreProfile allAverage.
	(self
		addMovieTitled: 'TRON (1982)'
		withImdbUrl: 'https://www.imdb.com/title/tt0084827/?ref_=tt_rec_tti'
		andImageUrl:
			'https://m.media-amazon.com/images/M/MV5BMzZhNjYyZDYtZmE4MC00M2RlLTlhOGItZDVkYTVlZTYxOWZlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY1000_CR0,0,666,1000_AL_.jpg') genreProfile: GenreProfile allAverage..
	(self
		addMovieTitled: 'John Carter (2012)'
		withImdbUrl: 'https://www.imdb.com/title/tt0401729/?ref_=tt_rec_tti'
		andImageUrl:
			'https://m.media-amazon.com/images/M/MV5BMDEwZmIzNjYtNjUwNS00MzgzLWJiOGYtZWMxZGQ5NDcxZjUwXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SY1000_CR0,0,728,1000_AL_.jpg') genreProfile: GenreProfile allAverage..
	self
		addMovieTitled: 'Clash of the Titans (2010)'
		withImdbUrl: 'https://www.imdb.com/title/tt0800320/?ref_=tt_rec_tti'
		andImageUrl:
			'https://m.media-amazon.com/images/M/MV5BMTYxNjg4MzU5OV5BMl5BanBnXkFtZTcwOTA3NTUwMw@@._V1_SY1000_CR0,0,675,1000_AL_.jpg'