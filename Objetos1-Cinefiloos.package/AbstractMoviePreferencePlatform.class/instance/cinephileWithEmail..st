accessing
cinephileWithEmail: anEmail
	^ self cinephiles detect: [ :each | each email = anEmail ] ifNone: [ nil ]