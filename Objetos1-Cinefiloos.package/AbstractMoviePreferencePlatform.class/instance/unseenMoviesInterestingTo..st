querying
unseenMoviesInterestingTo: aCinephile
	| alreadyWatched |
	alreadyWatched := aCinephile watchedMovies.
	^ self movies
		select: [ :each | (each genreSimilarityIndexTo: aCinephile) < 6 & (alreadyWatched includes: each) not ]