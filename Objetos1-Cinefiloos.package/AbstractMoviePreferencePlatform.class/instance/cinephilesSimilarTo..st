querying
cinephilesSimilarTo: aCinephile
	^ self cinephiles select: [ :each | (each genreSimilarityIndexTo: aCinephile) < 6 ]