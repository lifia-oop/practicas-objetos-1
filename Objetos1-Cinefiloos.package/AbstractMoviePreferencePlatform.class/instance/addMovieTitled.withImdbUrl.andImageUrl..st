factory
addMovieTitled: theTitle withImdbUrl: theImdbUrl andImageUrl: theImageUrl
	| movie |
	movie := Movie title: theTitle imdbUrl: theImdbUrl imageUrl: theImageUrl.
	self addMovie: movie.
	^ movie