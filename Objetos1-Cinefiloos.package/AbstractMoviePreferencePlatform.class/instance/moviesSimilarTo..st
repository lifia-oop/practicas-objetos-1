querying
moviesSimilarTo: aMovie
	^ self movies select: [ :each | (each genreSimilarityIndexTo: aMovie) < 6 ]