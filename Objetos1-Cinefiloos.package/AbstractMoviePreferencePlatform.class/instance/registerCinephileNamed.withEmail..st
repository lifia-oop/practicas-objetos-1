hooks
registerCinephileNamed: fullname withEmail: email
	| cinephile |
	cinephile := Cinephile fullname: fullname email: email.
	self addCinephile: cinephile.
	^ cinephile