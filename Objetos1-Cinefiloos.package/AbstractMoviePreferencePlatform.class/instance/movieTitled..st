accessing
movieTitled: aTitle
	^ self movies detect: [ :each | each title = aTitle ] ifNone: [ nil ]