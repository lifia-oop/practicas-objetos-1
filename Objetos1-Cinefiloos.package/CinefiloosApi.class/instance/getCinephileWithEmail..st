requests
getCinephileWithEmail: anEmail
	<get>
	<path: '/cinephiles?email={anEmail}'>
	self respondOkWith: (self cinephileWithEmail: anEmail) asJson