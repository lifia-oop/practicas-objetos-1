private
cinephile: aCinephileEmail watched: aMovieTitle
	| platform cinephile movie |
	platform := VoyageBasedMoviePreferencesPlatform new.
	cinephile := platform cinephileWithEmail: aCinephileEmail.
	cinephile ifNil: [ ^ self ].
	movie := platform movieTitled: aMovieTitle.
	movie ifNil: [ ^ self ].
	cinephile watched: movie.
	cinephile save.