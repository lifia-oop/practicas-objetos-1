rendering
renderAllMoviesButtonOn: html
	| item |
	item := html tbsLinkifyListGroupItem.
	selectedComponent == allMoviesComponent
		ifTrue: [ item beActive ].
	item
		callback: [ allMoviesComponent movies: platform movies.
			selectedComponent := allMoviesComponent ];
		with: 'All movies'.
	^ item