rendering
renderAddMovieButtonOn: html
	| item |
	item := html tbsLinkifyListGroupItem.
	selectedComponent == addMocv
		ifTrue: [ item beActive ].
	^ item
		callback: [ selectedComponent := addMocv ];
		with: 'Add a new movie'