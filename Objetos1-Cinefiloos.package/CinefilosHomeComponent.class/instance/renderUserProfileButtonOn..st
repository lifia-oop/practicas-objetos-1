rendering
renderUserProfileButtonOn: html
	| item |
	item := html tbsLinkifyListGroupItem.
	selectedComponent == userProfileComponent
		ifTrue: [ item beActive ].
	^ item
		callback: [ selectedComponent := userProfileComponent ];
		with: 'User profile'