initialize - release
initialize
	super initialize.
	platform := VoyageBasedMoviePreferencesPlatform new.
	platform movies ifEmpty: [ platform createExamples ].
	allMoviesComponent := MovieListComponent
		parent: self
		movies: platform movies.
	userProfileComponent := UserProfileComponent parent: self.
	addMocv := AddMovieComponent parent: self.
	similarMoviesComponents := OrderedCollection new.
	selectedComponent := allMoviesComponent