rendering
renderContentOn: html
	self renderHeaderOn: html.
	user
		ifNil: [ html tbsAlert
				beWarning;
				with: [ html strong: 'Log-in first!'.
					html text: ' To use this system you first need to log-in' ] ]
		ifNotNil: [ html tbsRow
				with: [ html tbsColumn: [ self renderNavigationOn: html ] mediumSize: 2.
					html tbsColumn: [ html render: selectedComponent ] mediumSize: 10 ] ]