rendering
renderHeaderOn: html
	html tbsNavbar
		beDefault;
		with: [ html
				tbsContainer: [ html
						tbsNavbarHeader: [ html tbsNavbarBrand
								url: '/cinefiloos';
								with: 'Cinefiloos' ].
					html
						form: [ user
								ifNil: [ html tbsNavbarButton
										tbsPullRight;
										callback: [ self login ];
										with: 'Log-in' ]
								ifNotNil: [ html tbsNavbarButton
										tbsPullRight;
										callback: [ user := nil ];
										with: 'Log-out' ] ] ] ]