rendering
renderContentOn: html
	errorMessage
		ifNotNil: [ html tbsAlert
				beDanger;
				with: [ html strong: 'Error! '.
					html text: errorMessage.
					errorMessage := nil.
					email := '' ] ].
	html
		tbsRow: [ html tbsColumn
				mediumSize: 4;
				mediumOffset: 1;
				with: [ self renderLoginPanelOn: html ].
			html tbsColumn
				mediumSize: 4;
				with: [ self renderRegistrationPanelOn: html ] ]