callbacks
checkCredentials
	| cinephile |
	email isEmpty
		ifFalse: [ cinephile := platform cinephileWithEmail: email.
			cinephile ifNil: [ errorMessage := email, ' is not a registered email' ] ifNotNil: [ self answer: cinephile ] ]