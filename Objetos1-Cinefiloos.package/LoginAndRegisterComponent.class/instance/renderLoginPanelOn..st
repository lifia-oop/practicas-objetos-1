rendering
renderLoginPanelOn: html
	html tbsPanel beDefault
		with: [ html tbsPanelHeading: 'If you have an account, use your email to log-in'.
			html
				tbsPanelBody: [ html
						tbsForm: [ html
								tbsFormGroup: [ html label
										for: 'email';
										with: 'Email address'.
									html textInput
										on: #email of: self;
										tbsFormControl;
										id: 'email';
										placeholder: 'Enter your email to log in' ].
							html tbsButton
								beSmall;
								beDefault;
								callback: [ self checkCredentials ];
								with: 'Login' ] ] ]