callbacks
registerAsNewUser
	| cinephile |
	email isEmpty | fullname isEmpty
		ifTrue: [ errorMessage := 'Both fields are mandatory'.
			^ self ].
	cinephile := platform cinephileWithEmail: email.
	cinephile
		ifNotNil: [ errorMessage := email , ' has already been registered'.
			^ self ].
	self answer: (platform registerCinephileNamed: fullname withEmail: email)