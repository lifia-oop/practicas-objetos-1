rendering
renderRegistrationPanelOn: html
	html tbsPanel beDefault
		with: [ html tbsPanelHeading: 'If you do not have an account, register with your name and email'.
			html
				tbsPanelBody: [ html
						tbsForm: [ html
								tbsFormGroup: [ html label
										for: 'fullname';
										with: 'Full Name'.
									html textInput
										on: #fullname of: self;
										tbsFormControl;
										id: 'fullname';
										placeholder: 'Your Full Name' ].
							html
								tbsFormGroup: [ html label
										for: 'email';
										with: 'Email address'.
									html textInput
										on: #email of: self;
										tbsFormControl;
										id: 'email';
										placeholder: 'you@yourdomain.com' ].
							html tbsButton
								beSmall;
								beDefault;
								callback: [ self registerAsNewUser ];
								with: 'Register' ] ] ]