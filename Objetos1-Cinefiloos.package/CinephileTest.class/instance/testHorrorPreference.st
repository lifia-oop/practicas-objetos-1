testing
testHorrorPreference
	| cinephile aHorrorMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	self assert: cinephile horrorPreference equals: 4.5.
	aHorrorMovie := Movie
		title: 'a horror movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justHorror.
	cinephile watched: aHorrorMovie.
	self assert: cinephile horrorPreference equals: 6.75