testing
testActionPreference
	| cinephile theMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	self assert: cinephile horrorPreference equals: 4.5.
	theMovie := Movie
		title: 'an action movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justAction.
	cinephile watched: theMovie.
	self assert: cinephile actionPreference equals: 6.75