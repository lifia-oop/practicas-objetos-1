testing
testRomancePreference
	| cinephile aRomanceMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	self assert: cinephile romancePreference equals: 4.5.
	aRomanceMovie := Movie
		title: 'a romance movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justRomance.
	cinephile watched: aRomanceMovie.
	self assert: cinephile romancePreference equals: 6.75