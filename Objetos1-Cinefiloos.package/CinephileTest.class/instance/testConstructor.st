testing
testConstructor
	| cinephile |
	cinephile := Cinephile fullname: 'Juan de Los Paloes' email: 'juan@email.com'.
	self assert: cinephile fullname equals: 'Juan de Los Paloes'.
	self assert: cinephile email equals: 'juan@email.com'.
	self assert: cinephile genreProfile equals: GenreProfile allAverage .
	self assert: cinephile watchedMovies isEmpty.