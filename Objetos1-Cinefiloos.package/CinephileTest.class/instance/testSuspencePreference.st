testing
testSuspencePreference
	| cinephile aSuspenseMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	self assert: cinephile suspensePreference equals: 4.5.
	aSuspenseMovie := Movie
		title: 'a sci-fi movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justSuspense.
	cinephile watched: aSuspenseMovie.
	self assert: cinephile suspensePreference equals: 6.75