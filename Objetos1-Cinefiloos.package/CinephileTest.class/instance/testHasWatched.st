testing
testHasWatched
	| cinephile aHorrorMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	aHorrorMovie := Movie
		title: 'a horror movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justHorror.
	self assert: cinephile genreProfile equals: GenreProfile allAverage.
	self deny: (cinephile hasWatched: aHorrorMovie).
	cinephile watched: aHorrorMovie.
	self assert: (cinephile hasWatched: aHorrorMovie).
	self
		assert: cinephile genreProfile
		equals:
			(GenreProfile
				asAnAverageOfAll:
					{GenreProfile allAverage.
					GenreProfile justHorror})