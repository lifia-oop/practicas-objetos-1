testing
testGenreSimilarityIndex
	| anAverageComedy ana juan |
	anAverageComedy := Movie
		title: 'The funny movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile:
			(GenreProfile allAverage
				comedy: 9;
				yourself).
	ana := Cinephile fullname: 'Ana' email: ''.
	juan := Cinephile fullname: 'Juan' email: ''.
	self assert: (ana genreSimilarityIndexTo: juan) equals: 0.
	ana watched: anAverageComedy.
	self assert: (ana genreSimilarityIndexTo: juan) equals: 2.25.
	self assert: (juan genreSimilarityIndexTo: ana) equals: 2.25