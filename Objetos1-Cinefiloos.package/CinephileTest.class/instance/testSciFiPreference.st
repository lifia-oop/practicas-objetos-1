testing
testSciFiPreference
	| cinephile aSciFiMovie |
	cinephile := Cinephile fullname: 'Juan de Los Palotes' email: 'juan@email.com'.
	self assert: cinephile sciFiPreference equals: 4.5.
	aSciFiMovie := Movie
		title: 'a sci-fi movie'
		imdbUrl: ''
		imageUrl: ''
		genreProfile: GenreProfile justSciFi.
	cinephile watched: aSciFiMovie.
	self assert: cinephile sciFiPreference equals: 6.75