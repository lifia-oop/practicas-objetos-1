testing
testbeAllMax
	| profile |
	profile := GenreProfile new.
	profile beAllMax.
	self assert: profile horror equals: 9.
	self assert: profile action equals: 9.
	self assert: profile romance equals: 9.
	self assert: profile suspense equals: 9.
	self assert: profile comedy equals: 9.
	self assert: profile sciFi equals: 9