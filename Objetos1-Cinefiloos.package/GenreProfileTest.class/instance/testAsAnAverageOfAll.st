testing
testAsAnAverageOfAll
	| result |
	result := GenreProfile
		asAnAverageOfAll:
			{GenreProfile allZero.
			GenreProfile allZero.
			GenreProfile allMax}.
	self assert: result horror equals: 3.
	self assert: result action equals: 3.
	self assert: result romance equals: 3.
	self assert: result suspense equals: 3.
	self assert: result comedy equals: 3.
	self assert: result sciFi equals: 3