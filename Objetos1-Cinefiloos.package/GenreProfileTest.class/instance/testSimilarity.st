testing
testSimilarity
	| horror allAverage allZero |
	horror := GenreProfile justHorror.
	allAverage := GenreProfile allAverage.
	allZero := GenreProfile allZero.
	self assert: (horror similarityIndexTo: horror) equals: 0.
	self assert: (horror similarityIndexTo: allZero) equals: 9.
	self assert: (allZero similarityIndexTo: horror) equals: 9.
	self assert: (allZero similarityIndexTo: allAverage) equals: 27