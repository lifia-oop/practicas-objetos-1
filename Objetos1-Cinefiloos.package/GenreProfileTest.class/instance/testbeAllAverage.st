testing
testbeAllAverage
	| profile |
	profile := GenreProfile new.
	profile beAllAverage.
	self assert: profile horror equals: 4.5.
	self assert: profile action equals: 4.5.
	self assert: profile romance equals: 4.5.
	self assert: profile suspense equals: 4.5.
	self assert: profile comedy equals: 4.5.
	self assert: profile sciFi equals: 4.5