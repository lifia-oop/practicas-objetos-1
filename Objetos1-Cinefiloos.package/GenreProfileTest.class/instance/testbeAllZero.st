testing
testbeAllZero
	| profile |
	profile := GenreProfile new.
	profile beAllZero.
	self assert: profile horror equals: 0.
	self assert: profile action equals: 0.
	self assert: profile romance equals: 0.
	self assert: profile suspense equals: 0.
	self assert: profile comedy equals: 0.
	self assert: profile sciFi equals: 0