testing
testEquals
	| profile |
	profile := GenreProfile new.
	self assert: GenreProfile new equals: GenreProfile new.
	profile horror: 4.
	self deny: profile = GenreProfile new.
	profile := GenreProfile new.
	profile action: 4.
	self deny: profile = GenreProfile new.
	profile := GenreProfile new.
	profile romance: 4.
	self deny: profile = GenreProfile new.
	profile := GenreProfile new.
	profile suspense: 4.
	self deny: profile = GenreProfile new.
	profile := GenreProfile new.
	profile comedy: 4.
	self deny: profile = GenreProfile new.
	profile := GenreProfile new.
	profile sciFi: 4.
	self deny: profile = GenreProfile new