testing
testConstructor
	| profile |
	profile := GenreProfile new.
	self assert: profile horror equals: 0.
	self assert: profile action equals: 0.
	self assert: profile romance equals: 0.
	self assert: profile suspense equals: 0.
	self assert: profile comedy equals: 0.
	self assert: profile sciFi equals: 0.
	profile := GenreProfile allAverage.
	self assert: profile horror equals: 4.5.
	self assert: profile action equals: 4.5.
	self assert: profile romance equals: 4.5.
	self assert: profile suspense equals: 4.5.
	self assert: profile comedy equals: 4.5.
	self assert: profile sciFi equals: 4.5