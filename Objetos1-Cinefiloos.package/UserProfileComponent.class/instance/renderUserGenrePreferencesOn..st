rendering
renderUserGenrePreferencesOn: html
	html heading
		level: 3;
		with: 'Genre preferences'.
	html
		paragraph: [ html
				strong: 'Horror: ';
				text: self user horrorPreference printString;
				break;
				strong: 'Comedy: ';
				text: self user comedyPreference printString;
				break;
				strong: 'Action: ';
				text: self user actionPreference printString;
				break;
				strong: 'Romance: ';
				text: self user romancePreference printString;
				break;
				strong: 'Suspense: ';
				text: self user suspensePreference printString;
				break;
				strong: 'Sci-Fi ';
				text: self user sciFiPreference printString;
				break ]