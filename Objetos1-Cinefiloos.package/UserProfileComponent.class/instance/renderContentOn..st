rendering
renderContentOn: html
	html
		tbsRow: [ html tbsColumn
				mediumSize: 6;
				with: [ self renderBasicUserDetailsOn: html.
					self renderUserGenrePreferencesOn: html ] ].
	html
		tbsRow: [ html tbsColumn
				mediumSize: 6;
				with: [ self renderWatchedMoviesListOn: html ].
			html tbsColumn
				mediumSize: 6;
				with: [ self renderRecommendedMoviesOn: html ] ]