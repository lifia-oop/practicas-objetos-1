rendering
renderBasicUserDetailsOn: html

	html heading
		level: 2;
		with: self user fullname.
	^ html
		paragraph: [ html
				strong: 'e-mail: ';
				text: self user email ]