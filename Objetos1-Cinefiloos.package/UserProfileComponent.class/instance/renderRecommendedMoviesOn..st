rendering
renderRecommendedMoviesOn: html
	html heading
		level: 3;
		with: 'Recommended movies'.
	html
		unorderedList: [ (self platform unseenMoviesInterestingTo: self user)
				do: [ :each | html listItem: each title ] ]