rendering
renderWatchedMoviesListOn: html
	html heading
		level: 3;
		with: 'Watched movies'.
	html unorderedList: [ self user watchedMovies do: [ :each | html listItem: each title ] ]