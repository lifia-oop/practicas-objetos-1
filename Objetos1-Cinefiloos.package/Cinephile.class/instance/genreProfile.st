accessing
genreProfile
	| toAverage |
	^ watchedMovies isEmpty
		ifTrue: [ GenreProfile allAverage ]
		ifFalse: [ toAverage := watchedMovies collect: [ :each | each genreProfile ].
			toAverage add: GenreProfile allAverage.
			GenreProfile asAnAverageOfAll: toAverage ]