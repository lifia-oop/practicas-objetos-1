converting
jsonOn: aJsonCanvas
	aJsonCanvas
		object: [ aJsonCanvas
				key: 'fullName' value: fullName;
				key: 'email' value: email;
				key: 'watchedMovies'
					value: (watchedMovies collect: [ :each | each title ]) ]