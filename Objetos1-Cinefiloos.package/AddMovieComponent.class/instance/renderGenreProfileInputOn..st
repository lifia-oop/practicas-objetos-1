rendering
renderGenreProfileInputOn: html
	html label: 'Horror: '.
	html space.
	html textInput
		on: #horror of: self;
		size: 4.
	html space.
	html label: 'Action: '.
	html space.
	html textInput
		on: #action of: self;
		size: 4.
	html space.
	html label: 'Romance: '.
	html space.
	html textInput
		on: #romance of: self;
		size: 4.
	html space.
	html label: 'Suspense: '.
	html space.
	html textInput
		on: #suspense of: self;
		size: 4.
	html space.
	html label: 'Comedy: '.
	html textInput
		on: #comedy of: self;
		size: 4.
	html space.
	html label: 'Sci-Fi: '.
	html space.
	html textInput
		on: #sciFi of: self;
		size: 4