rendering
renderContentOn: html
	html
		tbsForm: [ html
				tbsColumn: [ html tbsRow: [ self renderBasicMovieInfoOn: html ].
					html tbsRow: [ self renderGenreProfileInputOn: html ].
					html break.
					html
						tbsRow: [ html tbsButton
								beSmall;
								beDefault;
								callback: [ self addMovie ];
								with: 'Add' ] ]
				mediumSize: 10 ]