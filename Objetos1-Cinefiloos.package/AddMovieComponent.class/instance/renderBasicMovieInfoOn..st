rendering
renderBasicMovieInfoOn: html
	html
		tbsFormGroup: [ html label
				for: 'title';
				with: 'Movie title'.
			html textInput
				on: #title of: self;
				tbsFormControl;
				id: 'title';
				placeholder: 'The title of the Movie' ].
	html
		tbsFormGroup: [ html label
				for: 'imdbUrl';
				with: 'IMDB Url'.
			html textInput
				on: #imdbUrl of: self;
				tbsFormControl;
				id: 'imdbUrl';
				placeholder: 'the url of this movie in IMDB' ].
	html
		tbsFormGroup: [ html label
				for: 'imageUrl';
				with: 'Image Url'.
			html textInput
				on: #imageUrl of: self;
				tbsFormControl;
				id: 'imegeUrl';
				placeholder: 'the url of an image for this movie' ]