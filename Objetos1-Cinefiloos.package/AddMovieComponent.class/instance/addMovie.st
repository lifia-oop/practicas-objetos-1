callbacks
addMovie
	| movie |
	title notEmpty & imageUrl notEmpty & imdbUrl notEmpty
		ifTrue: [ movie := self platform addMovieTitled: title withImdbUrl: imdbUrl andImageUrl: imageUrl.
			movie
				genreProfile:
					(GenreProfile
						horror: horror asNumber
						action: action asNumber
						romance: romance asNumber
						suspense: suspense asNumber
						comedy: comedy asNumber
						sciFi: sciFi asNumber).
			movie save.
			title := ''.
			imageUrl := ''.
			imdbUrl := '' ]