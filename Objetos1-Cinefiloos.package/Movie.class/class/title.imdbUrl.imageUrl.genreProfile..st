instance creation
title: theTitle imdbUrl: theImdbUrl imageUrl: theImageUrl genreProfile: aCFGenreProfile
	^ self new
		title: theTitle
			imdbUrl: theImdbUrl
			imageUrl: theImageUrl
			genreProfile: aCFGenreProfile;
		yourself