instance creation
title: theTitle imdbUrl: theImdbUrl imageUrl: theImageUrl
	^ self new
		title: theTitle imdbUrl: theImdbUrl imageUrl: theImageUrl;
		yourself