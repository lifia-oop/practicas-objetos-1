testing
testUnschedule
	| scheduler |
	scheduler := self newFifoScheduller.
	self scheduleJobsIn: scheduler.
	scheduler unschedule: highestPriorityJob.
	self deny: (scheduler jobs includes: highestPriorityJob)