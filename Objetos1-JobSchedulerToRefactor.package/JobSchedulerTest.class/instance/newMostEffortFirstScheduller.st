factory methods
newMostEffortFirstScheduller
	| scheduler |
	scheduler := JobScheduler new.
	scheduler strategy: 'MostEffortFirst'.
	^ scheduler