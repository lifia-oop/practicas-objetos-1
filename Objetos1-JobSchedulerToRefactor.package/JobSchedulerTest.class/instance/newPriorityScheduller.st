factory methods
newPriorityScheduller
	| scheduler |
	scheduler := JobScheduler new.
	scheduler strategy: 'Priority'.
	^ scheduler