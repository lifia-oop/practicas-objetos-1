factory methods
newLifoScheduller
	| scheduler |
	scheduler := JobScheduler new.
	scheduler strategy: 'LIFO'.
	^ scheduler