factory methods
newFifoScheduller
	| scheduler |
	scheduler := JobScheduler new.
	scheduler strategy: 'FIFO'.
	^ scheduler