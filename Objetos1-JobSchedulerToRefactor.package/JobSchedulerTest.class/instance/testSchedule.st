testing
testSchedule
	| scheduler |
	scheduler := self newFifoScheduller.
	scheduler schedule: highestPriorityJob.
	self assert: (scheduler jobs includes: highestPriorityJob)