testing
testNext
	| scheduler |
	scheduler := self newFifoScheduller.
	self scheduleJobsIn: scheduler.
	self assert: scheduler next == firstJob.
	self assert: scheduler jobs size == 3.
	scheduler := self newLifoScheduller.
	self scheduleJobsIn: scheduler.
	self assert: scheduler next == lastJob.
	self assert: scheduler jobs size == 3.
	scheduler := self newPriorityScheduller.
	self scheduleJobsIn: scheduler.
	self assert: scheduler next == highestPriorityJob.
	self assert: scheduler jobs size == 3.
	scheduler := self newMostEffortFirstScheduller.
	self scheduleJobsIn: scheduler.
	self assert: scheduler next == mostEffortJob.
	self assert: scheduler jobs size == 3